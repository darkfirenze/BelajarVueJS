/**
 * Created by kucingmint on 11/14/16.
 */

var VueHelloWord = new Vue({
    el: "#app_1",
    data: {
        pesan_hello_world: "Hello World Vue JS"
    }
});


var VueAppBind1 = new Vue({
    el: "#app_2",
    data: {
        message_2: "Menampilkan tanggal halaman " + new Date()
    }
});


var vueAppDaftar = new Vue({
    el: "#app_4",
    data: {
        daftars : [
            {text: "Belajar js"},
            {text: "Belajar css"},
            {text: "Belajar html"},
            {text: "Belajar directive"},
            {text: "Belajar vue js"}
        ]
    }
});


var vueAppDaftarUl = new Vue({
    el: "#app_ul_list4",
    data: {
        lists : [
            {nama: "Nama 1"},
            {nama: "Nama 2"},
            {nama: "Nama 3"},
            {nama: "Nama 4"},
            {nama: "Nama 5"},
            {nama: "Nama 6"}
        ]
    }
});


var app5 = new Vue({
    el: "#app_5",
    data: {
        pesan: "Pesan yang akan Dibalik Nih"
    },
    methods: {
        balikkanPesan: function () {
            this.pesan = this.pesan.split("").reverse().join("")
        }
    }
});


var app5x = new Vue({
    el: "#app_5x",
    data: {
        pesanbalik: "Contoh balikan pesan"
    },
    methods: {
        fungsiBalikan: function () {
            this.pesanbalik = this.pesanbalik.split("").reverse().join("")
        }
    }
});


















